package org.surfonthewaves.projects.signature;

import java.io.*;
import java.security.*;
import java.security.cert.X509Certificate;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Main {

    public static void main(String[] args) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

//        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
//        InputStream certificateInputStream = new FileInputStream("/home/firstuser/CAsert/client.crt");
//        X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(certificateInputStream);
//        PublicKey publicKey = certificate.getPublicKey();

        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] keyStorePassword = "changeit".toCharArray();
        try(InputStream keyStoreData = new FileInputStream("/home/firstuser/CAsert/keystore.p12")){
            keyStore.load(keyStoreData, keyStorePassword);
        }

        KeyStore.ProtectionParameter entryPassword =
                new KeyStore.PasswordProtection(keyStorePassword);

        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)
                keyStore.getEntry("1", entryPassword);

        PrivateKey privateKey = privateKeyEntry.getPrivateKey();
        X509Certificate x509Certificate = (X509Certificate) privateKeyEntry.getCertificate();

//      random number generator
        SecureRandom secureRandom = new SecureRandom();
//
//You cannot use SHA1withDSA with an RSA key. Change the signature algorithm on SHA1withRSA or create a DSA-key key
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initSign(privateKey, secureRandom);

        byte[] data = "abcdefghijklmnopqrstuvxyz".getBytes("UTF-8");
        System.out.println( new String(data));

        signature.update(data);
        byte[] digitalSignature = signature.sign();

        System.out.println( new String(digitalSignature));

//      *********************************************************

        Signature signature1 = Signature.getInstance("SHA256WithRSA");
        signature1.initVerify(x509Certificate);

        signature1.update(data);

        boolean verified = signature1.verify(digitalSignature);
        System.out.println("verified = " + verified);

//      **********************************************************
    }

}
